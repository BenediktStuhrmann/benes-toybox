var gulp = require('gulp');
var pug = require('gulp-jade');
var sass = require('gulp-sass');

/* #### Pug to HTML and SCSS to CSS #### */
gulp.task('default', function() {
  //base (.pug --> .html)
  gulp.src('./pug/*.pug')
  .pipe(pug())
  .pipe(gulp.dest('./'));

  //modules (.pug --> .html)
  gulp.src('./pug/modules/*.pug')
  .pipe(pug())
  .pipe(gulp.dest('./modules'));

  //styles (.scss --> .css)
  return gulp.src('./resources/scss/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./resources/css'));
});
